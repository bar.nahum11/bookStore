let emitter = require('events').EventEmitter;
let em = new emitter();
module.exports = {
    em,
    mongoConnectionString : "mongodb://localhost:27017/BookStore",
    retDelegate : (res) => {
        return (err,data) =>{
            if(err){
                console.log(err)
                res.status(500).end(err.message)
            } else {
                res.json(data)
            }
        }
    },
    UPDATE_USERS_COUNT : "update_user_count",
    REQUEST_USER_COUNT : "request_user_count"
};
