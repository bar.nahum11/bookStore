const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.ObjectId;

const bookSchema = new Schema({
    _id: { type: ObjectId, auto: true},
    name : {type: String, required: true, unique : true},
    author : {type: String, required: true},
    price: {type: Number, required: true},
    summary: {type: String, required: false},
    publisher: {type: String, required: false},
    imgUrl : {type: String, required: false},
    category :{type : ObjectId, ref : 'Category', required:true}
},{
    versionKey: false
});

const Book = mongoose.model('Books', bookSchema);
module.exports = Book;