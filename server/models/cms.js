const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.ObjectId;

const CmsSchema = new Schema({
    _id: { type: ObjectId, auto: true},
    width: { type: Number, unique: true },
    depth: { type: Number, required: true },
    table: { type: Array, required: true }
});

CmsSchema.set('toJSON', { virtuals: true });

const Cms = mongoose.model('Cms', CmsSchema);
module.exports = Cms;