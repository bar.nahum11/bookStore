const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.ObjectId;

const orderSchema = new Schema({
    _id: { type: ObjectId, auto: true},
    price : {type: Number, required: true},
    user: {type : ObjectId, ref : 'User'},
    createdDate: { type: Date, default: Date.now },
    books: [{
        book: { type: Schema.Types.ObjectId, ref: 'Books'},
        quantity: { type: Number, default: 1 }
        }]
},{
    versionKey: false
});

const Order = mongoose.model('Order', orderSchema)
module.exports = Order;