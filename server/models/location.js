const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.ObjectId;

const locationSchema = new Schema({
    _id: { type: ObjectId, auto: true},
    name: {type: String, required: true, unique : true},
    location: {type: String, required: true}
},{
    versionKey: false
});

const Location = mongoose.model('Location', locationSchema)
module.exports = Location;