const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.ObjectId;

const userSchema = new Schema({
    _id: { type: ObjectId, auto: true},
    email: { type: String, unique: true, required: true },
    hashedPass: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
	isAdmin: { type: Boolean, require: true },
    createdDate: { type: Date, default: Date.now }
},{
    versionKey: false
});

userSchema.set('toJSON', { virtuals: true });

const User = mongoose.model('User', userSchema);
module.exports = User;