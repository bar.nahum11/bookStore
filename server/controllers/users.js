const dal = require('../db/usersDal');

module.exports = {
	authenticateUser(req, res, next) {
		dal.authenticate(req.body)
			.then(user => user ? res.json(user) : res.json({ message: 'Email or password is incorrect' }))
			.catch(err => next(err));
	},
	
	registerUser(req, res, next) {
		dal.createUser(req.body)
			.then(() => res.json({}))
			.catch(err => next(err));
	},

	isAdmin(req, res, next){
		dal.checkIfAdmin(req.body)
			.then(isAdmin => isAdmin ? res.json(user) : res.status(400).json({ message: 'user not found' }))
			.catch(err => next(err));
	},
	
	getAllUsers(req, res, next) {
		if(!req.user.sub.isAdmin) {
			res.status(403).send('Forbidden');
		}
		else
		{
			dal.getAllUsers()
				.then(users => res.json(users))
				.catch(err => next(err));
		}
	},
	
	getUserById(req, res, next) {
		if(!req.user.sub.isAdmin) {
			res.status(403).send('Forbidden');
		}
		else
		{ 
			dal.getUserById(req.params.id)
				.then(user => user ? res.json(user) : res.sendStatus(404))
				.catch(err => next(err));
		}
	},
	
	updateUser(req, res, next) {
		if(!req.user.sub.isAdmin) {
			res.status(403).send('Forbidden');
		}
		else
		{
			dal.updateUser(req.params.id, req.body)
				.then(() => res.json({}))
				.catch(err => next(err));
		}
	},
	
	_deleteUser(req, res, next) {
		if(!req.user.sub.isAdmin) {
			res.status(403).send('Forbidden');
		}
		else
		{
			dal.deleteUser(req.params.id)
				.then(() => res.json({}))
				.catch(err => next(err));
		}
	}
};
