const dal = require('../db/locationDal');
const {retDelegate} = require("../config")

module.exports = {
    locations: (req, res) => {
        const filter = req.body
        dal.getLocations(filter,retDelegate(res))
    }
};