const dal = require('../db/categoryDal');
const {retDelegate} = require("../config")

module.exports = {
    categories: (req, res) => {
        const filter = req.body
        dal.getCategories(filter,retDelegate(res))
    },

    addCategory: (req, res) => {
		const {name} = req.body;
		const newCategory = {name};
		dal.createCategory(newCategory, retDelegate(res))
    },

    category: (req, res) => {
        const {id} = req.params;
        dal.getCategoryById(id, retDelegate(res))
    },

    updateCategory: (req, res) => {
		const {id} = req.params;
		const{name} = req.body;
		const updatedCategory = {name};
		dal.updateCategory(id, updatedCategory, retDelegate(res))
    },

    deleteCategory: (req, res) => {
		const {id} = req.params;
		dal.deleteCategory(id, retDelegate(res))
	},
	
	numOfBooksPerCategory : (req,res) => {
		dal.numberOfBooksForCategory(retDelegate(res));
    }
};