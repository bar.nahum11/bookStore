const dal = require('../db/booksDal');
const countMinSketch = require("../helpers/countMinSketch");
const {retDelegate} = require("../config");
const AhoCorasick = require('ahocorasick');

module.exports = {
    books: (req, res) => {
		const filter = req.body;
        dal.getBooks(filter, retDelegate(res))
    },

    addBook: (req, res) => {
			const {
				name,author,price,
				summary,publisher,
				imgUrl,category
			} = req.body;
			const newBook = {
				name,author,price,
				summary,publisher,
				imgUrl,category
			};
		
			dal.createBook(newBook, retDelegate(res))
    },

    book: (req, res) => {
		const {id} = req.params;
		countMinSketch.updateSketch(id);
        dal.getBookById(id, retDelegate(res))
    },

    updateBook: (req, res) => {
			const {id} = req.params;
			const {
				name,author,price,
				summary,publisher,
				imgUrl,category
			} = req.body;
			const updatedBook = {
				name,author,price,
				summary,publisher,
				imgUrl,category
			};

			dal.updateBook(id, updatedBook, retDelegate(res))
    },

    deleteBook: (req, res) => {
			const {id} = req.params;
			dal.deleteBook(id, retDelegate(res))
	},
	
	booksPerCategory : (req,res) => {
		const {id} = req.params;
		dal.getBooks({category: id }, retDelegate(res));
    },

    bookByFreeText : (req,res) =>{
        const {freeText} = req.query;
        const splitted = freeText.split(";").filter(s => s!=="")
		const ac = new AhoCorasick(splitted);
        dal.getBooks({}, (err,books) => {
            const filteredbooks = books.filter(b=> {
                const dataToSearch = b.name + "@@" + b.author + "@@" + b.summary + "@@" + b.publisher + "@@" + b.category.name;
                const results = ac.search(dataToSearch);
                return results.length > 0
            });

            res.json(filteredbooks)
        });
	},
	bookIntrest : (req,res) =>{
        countMinSketch.getFrequency(req.params.id).then((value) => {
			res.json({value: value})
		});
    }
};
