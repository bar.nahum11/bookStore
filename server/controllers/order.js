const dal = require('../db/orderDal');
let {em} = require("../config")
const {retDelegate} = require("../config")

module.exports = {
    getAllOrders : (req,res) => {
      dal.getOrdersByUser({}, retDelegate(res))
    },

    getUserOrders: (req, res) => {
        const {userId} = req.params
        dal.getOrdersByUser({user : userId} ,retDelegate(res))
    },

    addOrder: (req, res) => {
        const {user, price, books} = req.body;
        const newOrder = {price, user, books};

        dal.createOrder(newOrder, retDelegate(res))
    },

    OrderById: (req, res) => {
        const {id} = req.params;
        dal.getOrderById(id, retDelegate(res))
    },

    deleteOrder: (req, res) => {
        const {id} = req.params;
        dal.deleteOrder(id, retDelegate(res))
    },

    numberOfOrdersForUser : (req,res) => {
        dal.numberOfOrdersForUser(retDelegate(res));
	}
};
