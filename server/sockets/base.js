let {UPDATE_USERS_COUNT,REQUEST_USER_COUNT} = require("../config")
module.exports = (io) => {

    let clientSockets = [];

    const emitUserCountToClient = ()=> {
        if(clientSockets){
            clientSockets.forEach(s=> s.emit(UPDATE_USERS_COUNT,clientSockets.length))
        }
    }

    io.on('connection', function (socket) {
        console.log('client connected');
        clientSockets.push(socket);
        emitUserCountToClient()

        socket.on('disconnect', function () {
            console.log('client disconnected');
            if (clientSockets.length > 0) {
                clientSockets.splice(clientSockets.indexOf(socket), 1);
                emitUserCountToClient();
            }
        });

        socket.on(REQUEST_USER_COUNT,() => {
            socket.emit(UPDATE_USERS_COUNT,clientSockets.length)
        });
    });
};
