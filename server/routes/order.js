var express = require('express');
var router = express.Router();
const controller = require('../controllers/order')

// order routes
router.get('/', controller.getAllOrders);
router.get('/user/:id', controller.getUserOrders);
router.post('/', controller.addOrder);
router.get('/:id', controller.OrderById);
router.delete('/:id', controller.deleteOrder);
router.get('/order/numberOfOrdersForUser', controller.numberOfOrdersForUser);

module.exports = router;
