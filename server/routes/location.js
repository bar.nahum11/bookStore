var express = require('express');
var router = express.Router();
const controller = require('../controllers/location')

// locations routes
router.get('/', controller.locations);

module.exports = router;
