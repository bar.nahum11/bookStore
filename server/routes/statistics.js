var express = require('express');
var router = express.Router();
const controller = require('../controllers/category')

// statistics routes
router.get('/numOfBooksPerCategory', controller.numOfBooksPerCategory);

module.exports = router;
