var express = require('express');
var router = express.Router();
const controller = require('../controllers/users')

// Users routes
router.post('/authenticate', controller.authenticateUser);
router.post('/register', controller.registerUser);
router.get('/user/:user', controller.getAllUsers);
router.get('/:id', controller.getUserById);
router.put('/:id', controller.updateUser);
router.delete('/:id', controller._deleteUser);
router.get('/isAdmin/:id',controller.isAdmin);

module.exports = router;
