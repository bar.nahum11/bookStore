var express = require('express');
var router = express.Router();
const controller = require('../controllers/category')

// category routes
router.get('/', controller.categories);
router.post('/', controller.addCategory);
router.get('/:id', controller.category);
router.put('/:id', controller.updateCategory);
router.delete('/:id', controller.deleteCategory);
router.get('/numOfBooksPerCategory', controller.numOfBooksPerCategory);

module.exports = router;
