var express = require('express');
var router = express.Router();
const controller = require('../controllers/book');

// book routes
router.get("/bookByFreeText/", controller.bookByFreeText);
router.get('/', controller.books);
router.post('/', controller.addBook);
router.get('/:id', controller.book);
router.put('/:id', controller.updateBook);
router.delete('/:id', controller.deleteBook);
router.get('/interest/:id', controller.bookIntrest);
router.get('/booksPerCategory/:id', controller.booksPerCategory);

module.exports = router;
