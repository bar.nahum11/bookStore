const bcrypt = require('bcryptjs');
const User = require('../models/users');
const { isAdmin } = require('../controllers/users');

module.exports = {
    async authenticate({email, password}) {
        const user = await User.findOne({email});
        
        if (user && bcrypt.compareSync(password, user.hashedPass)) {
            const { hashedPass, ...userWithouthashedPass} = user.toObject();
            return userWithouthashedPass;
        }

        return null;
    },
    
    async getAllUsers() {
        return await User.find().select('-hashedPass');
    },
    
    async getUserById(id) {
        return await User.findById(id).select('-hashedPass');
    },
    
    async createUser(userParam) {
        
        if (await User.findOne({email: userParam.email})) {
            throw 'email "' + userParam.email + '" already exisits';
        }
    
        const user = new User(userParam);
        
        user.isAdmin = false;
        
        if (userParam.password) {
            user.hashedPass = bcrypt.hashSync(userParam.password, 10);
        }
        
        await user.save();
    },
    
    async updateUser(id, userParam) {
        const user = await User.findById(id);
    
        if (!user) throw 'User not found';
        if (user.email !== userParam.email && await User.findOne({email: userParam.email})) {
            throw 'email "' + userParam.email + '" already exisits';        
        }
    
        if (userParam.password) {
            userParam.hashedPass = bcrypt. hashSync(userParam.password, 10);
        }
    
        Object.assign(user, userParam);
    
        await user.save();
    },
    
    async _deleteUser(id) {
        await User.findByIdAndRemove(id);
    },

    async checkIfAdmin(id){
        return await User.findById(id).select(isAdmin);
    }
    
};

//Users


