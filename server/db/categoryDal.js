const Category = require("../models/category");
const Book = require('../models/book');

module.exports = {
    createCategory: (jsonCategory, callback) => {
        let newCategory = new Category(jsonCategory);
        newCategory.save((err, category) => {
            if (callback) callback(err, category);
        });
    },
    getCategories: (filter, callback) => {
        Category.find(filter, (err, categories) => {
            if (callback) callback(err, categories);
        });
    },
    getCategoryById: (id, callback) => {
        Category.findById(id, (err, category) => {
            if (callback) callback(err, category);
        });
    },
    updateCategory: (id, categoryData, callback) => {
        Category.findByIdAndUpdate(id, categoryData, {new: true}, (err, category) => {
            if (callback) callback(err, category);
        })
    },
    deleteCategory: (id, callback) => {
        Book.remove({category: id}).exec()
        Category.findByIdAndDelete(id, (err, category) => {
            if (callback) callback(err, category);
        });
    },
    numberOfBooksForCategory: (cb) => {
        let retArray = [];
        const aggregatorOpts = [
            {
                $group: {
                    _id: '$category',

                    Count: {"$sum": 1},
                }
            },
            {
                $project: {
                    ID: '$_id',
                    Count: '$Count',
                }
            },
        ];
        Book.aggregate(aggregatorOpts, (err, data) => {
            data.forEach(d => {
                Category.findById(d.ID, (err, category) => {
                    retArray.push({name: category.name, count: d.Count})
                    if (retArray.length === data.length) {
                        cb(null, retArray)
                    }
                })
            });
        })
    }
};
