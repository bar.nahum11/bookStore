const Location = require("../models/location");

module.exports = {
    getLocations: (filter, callback) => {
        Location.find(filter, (err, categories) => {
            if (callback) callback(err, categories);
        });
    }
};
