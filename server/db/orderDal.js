const User = require("../models/users");
const Order = require("../models/order");


module.exports = {
    createOrder: (jsonOrder, callback) => {
        let newOrder = new Order(jsonOrder);
        newOrder.save((err, order) => {
            if (callback) callback(err, order);
        });
    },
    getOrdersByUser: (filter, callback) => {
        Order.find(filter).populate("books.book").populate("user").exec((err, orders) => {
            if (callback) callback(err, orders)
        })
    },
    getOrderById: (id, callback) => {
        Order.findById(id).populate("books.book").populate("user").exec((err, order) => {
            if (callback) callback(err, order);
        });
    },
    deleteOrder: (id, callback) => {
        Order.findByIdAndDelete(id, (err, order) => {
            if (callback) callback(err, order);
        });
    },
    numberOfOrdersForUser: (cb) => {
        let retArray = [];
        let o = {};
        o.map = function () { emit(this.user, 1) };
        o.reduce = function (k, vals) { return vals.length };
        Order.mapReduce(o, (err, data) => {
            data.results.forEach(r=>{
                User.findById(r._id,(err,user) =>{
                    retArray.push({email : user.email,count : r.value})
                    if(retArray.length === data.results.length){
                        cb(null, retArray)
                    }
                })
            });
        })
    }
};
