const mongoose = require('mongoose');

module.exports = {
    db: null,
    connect: (uri, callback) => {
        mongoose.connect(uri, {useNewUrlParser: true});
        db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', () => {
            console.log('We have connected to mongodb');
            if (callback) callback();
        });
    },

    close: () => {
        db.close();
    }
};