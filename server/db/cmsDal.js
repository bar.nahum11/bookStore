const Cms = require("../models/cms");

module.exports = {
    _setCms : async(sketch) => {
        const cms = new Cms(sketch.toJSON());
        await cms.save()
    },
    updateCms: async(sketch) => {
        const oldSketch = await Cms.findOne();
        Object.assign(oldSketch, sketch.toJSON());
        await oldSketch.save();
    },

    getCms: async() => {
        return await Cms.findOne();
    }
};
