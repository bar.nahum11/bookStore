const Book = require('../models/book');

module.exports = {
    createBook: (jsonBook, callback) => {
        let newBook = new Book(jsonBook);
        newBook.save((err, book) => {
            if (callback) callback(err, book);
        });
    },
    getBooks: (filter, callback) => {
        Book.find(filter).populate("category").exec((err, books) => {
            if (callback) callback(err, books)
        })
    },
    getBookById: (id, callback) => {
        Book.findById(id).populate("category").exec((err, book) => {
            if (callback) callback(err, book);
        });
    },
    updateBook: (id, bookData, callback) => {
        Book.findByIdAndUpdate(id, bookData, {new: true}, (err, book) => {
            if (callback) callback(err, book);
        }).populate("category");
    },
    deleteBook: (id, callback) => {
        Book.findByIdAndDelete(id, (err, book) => {
            if (callback) callback(err, book);
        });
    }
};
