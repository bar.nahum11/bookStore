const createError = require('http-errors');
const express = require('express');
const cors = require('cors')
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const categoryRouter = require('./routes/category');
const statisticsRouter = require('./routes/statistics');
const booksRouter = require('./routes/book');
const orderRouter = require('./routes/order');
const locationRouter = require('./routes/location');

var corsOptions = {
  origin: 'http://localhost:4200',
  credentials: true };

const app = express();

app.use(cors(corsOptions))
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/controllers/users', usersRouter);
app.use('/controllers/category',categoryRouter);
app.use('/controllers/statistics',statisticsRouter);
app.use('/controllers/book', booksRouter);
app.use('/controllers/order', orderRouter);
app.use('/controllers/location', locationRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(res);
  // render the error page
  res.status(err.status || 500);
  res.json({ error: err })
});

module.exports = app;
