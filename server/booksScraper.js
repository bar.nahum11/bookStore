const {Builder, By, Browser, Capabilities} = require('selenium-webdriver');
const chromedriver = require('chromedriver');
const dal = require('./db/dal');
const categoryDal = require('./db/categoryDal');
const booksDal = require('./db/booksDal');
const Category = require('./models/category');

tzomet();

async function tzomet() {
  var mongoDB = "mongodb://localhost:27017/BookStore";

  var chromeCapabilities = Capabilities.chrome();
  var chromeOptions = {'args': ['--test-type', '--incognito']};
  chromeCapabilities.set('chromeOptions', chromeOptions);
  dal.connect(mongoDB);
  let driver = await new Builder().forBrowser(Browser.CHROME).withCapabilities(chromeCapabilities).build();
  await driver.get('https://www.booknet.co.il/');
  let Categories = await (await driver.findElement(By.css('#categories-menu'))).findElements(By.tagName("li"));
  for(let categoryId = 0; categoryId < Categories.length; categoryId++) {
    if(categoryId != 1 && categoryId != 2 && categoryId != 15 && categoryId != 20) {
      let currCategory = await ((await (await driver.findElement(By.css('#categories-menu'))).findElements(By.tagName("li")))[categoryId]).findElement(By.tagName("a"));
      let currCategoryName = await currCategory.getText();
  
      var category = {
        name: currCategoryName
      };
      
      let newCategory = new Category(category);
      await newCategory.save((err, category) => {
          if(err) console.log("error" + err);
      });
  
      await currCategory.click();
      let productsInPage = await driver.findElements(By.css(".product"));
    
      for(let prodId = 0; prodId < productsInPage.length; prodId++) {
        try {
          let currProduct = (await driver.findElements(By.css(".product")))[prodId];
          let prodData = await currProduct.findElement(By.css(".leftInfo"));
          let name_author_desc = await prodData.findElement(By.tagName("a"));
          let name = await (await (await name_author_desc.findElement(By.css(".product-name"))).findElement(By.tagName("h3"))).getText();
          let author = await (await (await name_author_desc.findElement(By.css(".product-author"))).findElement(By.tagName("h4"))).getText();
          if(author && author != "") {
            let prices = await (await (await prodData.findElement(By.tagName("form"))).findElement(By.tagName("div"))).findElements(By.tagName("div"));
            let sitePrice = parseFloat((await prices[1].getText()).replace("מחיר באתר: ", "").replace(" ₪", ""));
          
            try{
              await name_author_desc.click();
            } catch {
              await (await currProduct.findElement(By.tagName("img"))).click();
            }
            
            await sleep(100);
            let publisher = (await ((await driver.findElements(By.css(".product-txt-general")))[1]).getText()).replace("הוצאה: ", "");
            console.log("הוצאה: " + publisher);
            let summary = await (await driver.findElement(By.css(".prodtxt-summery-txt"))).getText();
            let imgProduct = await (await (await driver.findElement(By.css(".bprodpic"))).findElement(By.tagName("img"))).getAttribute("src");
            await driver.navigate().back();
            var book = {
              name: name,
              author: author,
              price: sitePrice,
              summary: summary,
              publisher: publisher,
              imgUrl: imgProduct,
              category: newCategory._id
            };
            booksDal.createBook(book);
          }
        } catch {}
      }
    }
  }
}
  
async function sleep(ms){
  return new Promise(resolve=>{
      setTimeout(resolve,ms)
  })
}