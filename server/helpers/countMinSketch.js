const dal = require('../db/cmsDal');
var createCountMinSketch = require("count-min-sketch")
 
var sketch;
var cms;

async function updateSketch(key) {
	if(!sketch) {
		sketch = createCountMinSketch();
		cms = await dal.getCms();
		
		if(!cms) {
			await dal._setCms(sketch);
		}
	} else {
		cms = await dal.getCms();
	}
	
	sketch.fromJSON(cms);
	
	sketch.update(key, 1);
	
	await dal.updateCms(sketch);
}

async function getFrequency(key){
	if(!sketch) {
		sketch = createCountMinSketch();
		cms = await dal.getCms();
		
		if(!cms) {
			await dal._setCms(sketch);
		}
	
		sketch.fromJSON(cms);
	}

	return await sketch.query(key);
}
 
module.exports = {updateSketch, getFrequency};