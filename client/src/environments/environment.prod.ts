export const environment = {
  production: true,
  serverRoot: "http://localhost:3000/",
  apiRoot: "http://localhost:3000/controllers/"
};
