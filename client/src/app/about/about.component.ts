import { Component, OnInit } from '@angular/core';
import {LocationService} from '../services/location.service';
import {Location} from '../models/location.model'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  locations:Location[];

  constructor(private locationService:LocationService) 
  { 
    
  }

  async ngOnInit() {
    await this.locationService.getLocations().subscribe(res=> {
      this.locations = res;
      let map = window['createMap']();
      
      for(let loc of this.locations){
        window['addLocationToMap'](loc.name, loc.location, map);
      }  
    })
  }

}