import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './auth.guard';
import {BooksComponent} from './books/books.component';
import {BookComponent} from './book/book.component';
import {AboutComponent} from './about/about.component';
import {CategoriesComponent} from './categories/categories.component';
import {AdminPageComponent} from "./admin-page/admin-page.component";
import {CartComponent} from './cart/cart.component';
import {LoginComponent} from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CategoryComponent } from './category/category.component'

const routes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'book/:id', component: BookComponent },
  { path: 'about', component:AboutComponent},
  { path: 'categories',      component: CategoriesComponent},
  { path: 'categories/:id', component: CategoryComponent },
  { path: 'admin',      component: AdminPageComponent, data: {expectedRole: 'admin'},canActivate: [AuthGuard]},
  { path: 'cart',       component:CartComponent},
  { path: 'login',       component:LoginComponent},
  { path: 'register',       component:RegisterComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
