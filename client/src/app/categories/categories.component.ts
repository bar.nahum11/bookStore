import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { Category } from '../models/category.model';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ValidateActionModalComponent } from '../dialogs/validate-action-modal/validate-action-modal.component';
import { EditCategoryComponent } from '../dialogs/edit-category/edit-category.component';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { OkDialogComponent } from '../dialogs/ok-dialog/ok-dialog.component';

//Component specifications 
@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})

//exporting the categories component 
export class CategoriesComponent implements OnInit {
  categories: Category[];
  filtered_categories: Category[];
  newCategory:Category;
  logged_in_user: User;
  name_filter: string = "";

  btnDisabled = false;

  constructor(
    private categoryService: CategoryService,
    private router: Router,
    private dialog: MatDialog,
    private userService:UserService
  ) { }

  async ngOnInit() {
    this.userService.logged_in_user.subscribe(res=>this.logged_in_user = res)
    this.newCategory = new Category();
    try {
      this.categoryService.getCategories().subscribe(res => {this.categories = res; this.filtered_categories = res;})
    } catch (error) {
      console.log("error");
    }
  }

  filterCategories(){
    this.filtered_categories = this.categories.filter(item=>
      (!this.name_filter || item.name.indexOf(this.name_filter) > -1));
  }

  async addCategory() {
    this.btnDisabled = true;
    try {
      this.categoryService.createCategory(this.newCategory).subscribe(
                                result => {this.newCategory = new Category(); 
                                           this.dialog.open(OkDialogComponent, {data:"The category created successfully!"});
                                           this.categoryService.getCategories().subscribe(res => {this.categories = res;})});
    } catch (error) {
      console.log("error");
    }
    this.btnDisabled = false;
  }

  deleteCategory(id) {
    let deleteDialogRef = this.dialog.open(ValidateActionModalComponent);

    deleteDialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.categoryService.deleteCategory(id).subscribe(
                result => {this.categoryService.getCategories().subscribe(res => {this.categories = res;})});
      }
    });
  }

  editCategory(category)
  {
    let deleteDialogRef = this.dialog.open(EditCategoryComponent, {data:{...category}, width:'50%', height: '600px'});

    deleteDialogRef.afterClosed().subscribe(result => {
      if(result)
      {
        this.categoryService.getCategories().subscribe(res => {this.categories = res;});
      }  else if(result === null){
        this.dialog.open(OkDialogComponent, {data:"Error - category doesn't exists!"}).afterClosed().subscribe(()=>{
          this.router.navigate(['/']);
        });
      }
    });
  }

}
