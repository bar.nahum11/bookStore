import { User } from './user.model';
import { BookInOrder } from './bookInOrder.model';

export class Order {
    price: Number;
    user: User;
    createdDate: String;
    books: BookInOrder[];
}
