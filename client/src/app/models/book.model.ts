import { Category } from './category.model';

export class Book {
    _id: String;
    name: String;
    author: String;
    price: Number;
    summary: String;
    publisher: String;
    imgUrl: String;
    category: Category;
}
