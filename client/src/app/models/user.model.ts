export class User {
    email: String;
    firstName: String;
    lastName: String;
    password: String;
    isAdmin: boolean;
}
