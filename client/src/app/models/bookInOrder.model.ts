import { Book } from './book.model';

export class BookInOrder {
    book: Book;
    quantity: Number;
}
