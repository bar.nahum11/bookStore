import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {BookService} from '../services/book.service';
import {Book} from '../models/book.model';
import {Category} from '../models/category.model';

//component specifc details 
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})

//exporting the category component 
export class CategoryComponent implements OnInit {
  categoryId: any;
  category: Category;
  books: Book[];

  constructor(
    private bookService: BookService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.categoryId = res['id'];

      this.bookService.getBooks().subscribe(res => {
        this.books = res.filter(item=>
          (item.category._id.indexOf(this.categoryId) > -1))
      })
    });
  }
}
