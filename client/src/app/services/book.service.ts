import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BookService {
  URL_ROOT = environment.apiRoot + 'book/';

  constructor(private http:HttpClient) { }

  getBooks(): Observable<Book[]>
  {
    return this.http.get<Book[]>(this.URL_ROOT);
  }

  getBook(id:String): Observable<Book>{
    return this.http.get<Book>(this.URL_ROOT + "/" + id, {withCredentials:true})
  }

  getBookByCategory(catId:String): Observable<Book[]>
  {
    return this.http.get<Book[]>(this.URL_ROOT + "/booksPerCategory/" + catId);
  }

  deleteBook(id:String){
    return this.http.delete(this.URL_ROOT + "/" + id);
  }

  createBook(book:Book){
    return this.http.post(this.URL_ROOT, book);
  }

  editBook(book:Book){
    return this.http.put(this.URL_ROOT + "/" + book._id, book);
  }

  freeTextSearch(freeText:String): Observable<Book[]>{
    // If no free text was given return all rooms
    if(!freeText)
      return this.getBooks();
    return this.http.get<Book[]>(`${this.URL_ROOT}/bookByFreeText?freeText=${freeText}`)
  }

  getBookInterest(id:String){
    return this.http.get(this.URL_ROOT + "interest/" + id);
  }
}
