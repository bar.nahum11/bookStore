import { Injectable } from '@angular/core';
import { Location } from '../models/location.model';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  URL_ROOT = environment.apiRoot + 'location/';

  constructor(private http:HttpClient) { }

  getLocations(): Observable<Location[]>
  {
    return this.http.get<Location[]>(this.URL_ROOT)
  }
}
