import { Injectable } from '@angular/core';
import { Order } from '../models/order.model';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  URL_ROOT = environment.apiRoot + 'order/';

  constructor(private http:HttpClient) { }

  getOrders(): Observable<Order[]>
  {
    return this.http.get<Order[]>(this.URL_ROOT)
  }

  getOrder(id:String): Observable<Order>{
    return this.http.get<Order>(this.URL_ROOT + id, {withCredentials:true})
  }

  getUserOrders(userId:String): Observable<Order[]>{
    return this.http.get<Order[]>(this.URL_ROOT + "user/" + userId, {withCredentials:true})
  }

  deleteOrder(id:String){
    return this.http.delete(this.URL_ROOT + id);
  }

  createOrder(order:Order){
    return this.http.post(this.URL_ROOT, order);
  }

  getNumOfOrdersPerUser(): Observable<Object[]>{
    return this.http.get<Object[]>(this.URL_ROOT + "order/numberOfOrdersForUser");
  }
}
