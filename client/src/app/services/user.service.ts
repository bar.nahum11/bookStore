import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';


export const TOKEN_NAME: string = 'jwt_token';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  URL_ROOT = environment.apiRoot + 'users/';
  logged_in_user = new BehaviorSubject<User>(null);

  constructor(private http:HttpClient) { }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  unsetToken():void{
    localStorage.removeItem(TOKEN_NAME);
  }

  isAdmin(id: String): Observable<boolean>{
    let url = this.URL_ROOT + 'isAdmin/' + id;

    return this.http.get<boolean>(url);
  }

  registerUser(user: User){
    let url = this.URL_ROOT + 'register'

    return this.http.post(url, user);
  }

  loginUser(user: User): Observable<User>{
    let url = this.URL_ROOT + 'authenticate'

    return this.http.post<User>(url, user);
  }

  currentUser():Observable<User>{
    let url = this.URL_ROOT + 'current'
    return this.http.get<User>(url)
  }

  updateLoggedInUser(user: User){
    this.logged_in_user.next(user);
  }
}
