import { Injectable } from '@angular/core';
import { Category } from '../models/category.model';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  URL_ROOT = environment.apiRoot + 'category/';

  constructor(private http:HttpClient) { }

  getCategories(): Observable<Category[]>
  {
    return this.http.get<Category[]>(this.URL_ROOT)
  }

  getCategory(id:String): Observable<Category>{
    return this.http.get<Category>(this.URL_ROOT + "/" + id, {withCredentials:true})
  }

  deleteCategory(id:String){
    return this.http.delete(this.URL_ROOT + "/" + id);
  }

  createCategory(category:Category){
    return this.http.post(this.URL_ROOT, category);
  }

  editCategory(category:Category){
    return this.http.put(this.URL_ROOT + "/" + category._id, category);
  }
}
