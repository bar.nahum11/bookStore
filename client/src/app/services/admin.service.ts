import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  URL_STATISTICS = environment.apiRoot + 'statistics';
  constructor(private http:HttpClient) { }

  NumBooksPerCategory(){
    return this.http.get(this.URL_STATISTICS + '/numOfBooksPerCategory')
  }
}