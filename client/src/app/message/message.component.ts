import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  data: any;

  constructor(private cart: CartService) 
  { 
    this.data = cart;
  }

  ngOnInit() {
  }

}