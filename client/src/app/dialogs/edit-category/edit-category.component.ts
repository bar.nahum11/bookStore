import { Component, OnInit, Inject } from '@angular/core';
import { Category } from '../../models/category.model';
import { MatDialogRef } from '@angular/material/dialog';
import { CategoryService } from '../../services/category.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditCategoryComponent>, 
              private categoryService:CategoryService, 
              @Inject(MAT_DIALOG_DATA) public category: Category) { }

  ngOnInit() {
    
  }

  EditCategory()
  {
    this.categoryService.editCategory(this.category).subscribe(res=>this.dialogRef.close(res));
  }

}
