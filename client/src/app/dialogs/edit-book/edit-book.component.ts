import { Component, OnInit, Inject } from '@angular/core';
import { Book } from '../../models/book.model';
import { MatDialogRef } from '@angular/material/dialog';
import { BookService } from '../../services/book.service';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../models/category.model';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {
  allCategories:Category[]

  constructor(public dialogRef: MatDialogRef<EditBookComponent>,
              private categoryService:CategoryService, 
              private bookService:BookService, 
              @Inject(MAT_DIALOG_DATA) public book: Book) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(res=> this.allCategories = res)
  }

  EditBook()
  {
    this.bookService.editBook(this.book).subscribe(res=>this.dialogRef.close(res));
  }
}
