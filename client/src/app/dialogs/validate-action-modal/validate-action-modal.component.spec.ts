import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateActionModalComponent } from './validate-action-modal.component';

describe('ValidateActionModalComponent', () => {
  let component: ValidateActionModalComponent;
  let fixture: ComponentFixture<ValidateActionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateActionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateActionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
