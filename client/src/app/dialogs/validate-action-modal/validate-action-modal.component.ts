import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-validate-action-modal',
  templateUrl: './validate-action-modal.component.html',
  styleUrls: ['./validate-action-modal.component.scss']
})
export class ValidateActionModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ValidateActionModalComponent>) { }

  ngOnInit() {
  }

  Yes(){
    this.dialogRef.close(true);
  }

  No(){
    this.dialogRef.close(false);
  }
}
