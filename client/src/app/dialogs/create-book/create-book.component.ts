import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/book.model';
import { MatDialogRef } from '@angular/material/dialog';
import { BookService } from '../../services/book.service';
import { Category } from '../../models/category.model';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.scss']
})
export class CreateBookComponent implements OnInit {
  book:Book
  allCategories:Category[]
  constructor(public dialogRef: MatDialogRef<CreateBookComponent>, private bookService:BookService, 
                                                                   private categoryService:CategoryService) { }

  ngOnInit() {
    this.book = new Book();
    this.categoryService.getCategories().subscribe(res=> this.allCategories = res)
  }

  CreateBook(){
    this.bookService.createBook(this.book).subscribe(res=>this.dialogRef.close(res));
  }
}
