import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { CartService } from '../services/cart.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Order } from '../models/order.model';
import { UserService } from '../services/user.service';
import { BookInOrder } from '../models/bookInOrder.model';
import { MatDialog } from '@angular/material/dialog';
import { OkDialogComponent } from '../dialogs/ok-dialog/ok-dialog.component';


//componnet files specifications 
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})

//exporting the cart component 
export class CartComponent implements OnInit {
  btnDisabled = false;
  handler: any;

  quantities = [];

  constructor(
    private cart: CartService,
    private http:HttpClient,
    private router: Router,
    private orderService: OrderService,
    private user: UserService,
    private dialog: MatDialog
  ) {}

  trackByCartItems(index: number, item: any) {
    return item._id;
  }

  get cartItems() {
    return this.cart.getCart();
  }

  get cartTotal() {
    let total = 0;
    this.cartItems.forEach((data, index) => {
      total += data['price'] * this.quantities[index];
    });
    return Number(total.toFixed(2));
  }

  removeProduct(index, product) {
    this.quantities.splice(index, 1);
    this.cart.removeFromCart(product);
  }

  ngOnInit() {
    this.cartItems.forEach(data => {
      this.quantities.push(1);
    });
  }

  validate() {
    if (!this.quantities.every(data => data > 0)) {
      this.cart.warning('Quantity cannot be less than one.');
    }
    else {
      this.cart.message = '';
      return true;
    }
  }

  userValidate() {
    if (this.user.logged_in_user.value === null) {
      this.cart.warning('Please login to order');
    }
    else {
      this.cart.message = '';
      return true;
    }
  }

  pay() {
    this.btnDisabled = true;
    try {
      if (this.validate() && this.userValidate()) {
        let newOrder: Order;
        newOrder = new Order();
        newOrder.user = this.user.logged_in_user.value;
        newOrder.price = this.cartTotal;  
        newOrder.books = new Array<BookInOrder>(); 
        this.cartItems.forEach((book, index) => {
          newOrder.books.push({
            book: book,
            quantity: this.quantities[index],
          });
        });

        this.orderService.createOrder(newOrder).subscribe(
          result => { 
            this.dialog.open(OkDialogComponent, {data:"Order Sent!"});
            this.cart.clearCart();});;
      } else {
        this.btnDisabled = false;
      }
    } catch (error) {
      this.cart.error(error);
    }
  }

  checkout() {
    this.btnDisabled = true;
    try {
      if (this.validate()) {
        this.handler.open({
          name: 'Book House',
          description: 'Checkout Payment',
          amount: this.cartTotal * 100,
          closed: () => {
            this.btnDisabled = false;
          },
        });
      } else {
        this.btnDisabled = false;
      }
    } catch (error) {
      this.cart.error(error);
    }
  }
}
