import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BookComponent } from './book/book.component';
import { MessageComponent } from './message/message.component';
import { AboutComponent} from './about/about.component';
import { CategoriesComponent} from './categories/categories.component';
import { CategoryComponent } from './category/category.component';
import { CartComponent } from './cart/cart.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ValidateActionModalComponent } from './dialogs/validate-action-modal/validate-action-modal.component';
import { OkDialogComponent } from './dialogs/ok-dialog/ok-dialog.component'
import { EditCategoryComponent } from './dialogs/edit-category/edit-category.component';
import { CreateBookComponent } from './dialogs/create-book/create-book.component';
import { EditBookComponent } from './dialogs/edit-book/edit-book.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from '../environments/environment.prod'
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PieChartDirective } from './directives/pie-chart.directive';
import { AdminPageComponent } from './admin-page/admin-page.component';

const config: SocketIoConfig = { url: environment.serverRoot, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BookComponent,
    MessageComponent,
    AboutComponent,
    CategoriesComponent,
    CategoryComponent,
    ValidateActionModalComponent,
    OkDialogComponent,
    EditCategoryComponent,
    PieChartDirective,
    AdminPageComponent,
    CreateBookComponent,
    EditBookComponent,
    CartComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatDividerModule,
    MatCardModule,
    MatInputModule,
    HttpClientModule,
    FormsModule,
    MatDialogModule,
    MatOptionModule,
    MatSelectModule,
    SocketIoModule.forRoot(config),
    NgbModule,
    MatTableModule,
    MatSlideToggleModule,
    MatSliderModule
  ],
  entryComponents: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
