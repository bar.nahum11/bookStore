import { Component, OnInit } from '@angular/core';
import { BookService } from '../services/book.service';
import { Book } from '../models/book.model';
import { CreateBookComponent } from '../dialogs/create-book/create-book.component';
import { OkDialogComponent } from '../dialogs/ok-dialog/ok-dialog.component';
import { debounceTime } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  all_books: Book[];
  filtered_books: Book[];
  name_filter: string = "";
  min_price_filter: string = "";
  max_price_filter: string = "";
  author_filter: string = "";
  free_text_filter: string = "";
  freeSearch:Boolean = false;
  logged_in_user: User;

  constructor(private bookService: BookService,private dialog: MatDialog,private userService:UserService ) {
    
   }

  ngOnInit() {
    this.userService.logged_in_user.subscribe(res=>this.logged_in_user = res)
    this.bookService.getBooks().subscribe(res => {this.all_books = res; this.filtered_books=res})
  }

  CreateBook(){
    let createBookDialogRef = this.dialog.open(CreateBookComponent, {'width':'50%', 'height': '600px'});
    createBookDialogRef.afterClosed().subscribe(result=>{
      if(result){
        this.dialog.open(OkDialogComponent, {data:"The book created successfully!"});
        this.bookService.getBooks().subscribe(res => this.all_books = res);
      }
    });
  }

  filterBooks(){
    this.filtered_books = this.all_books.filter(item=>
      (!this.name_filter || item.name.indexOf(this.name_filter) > -1) && 
      (!this.author_filter || item.author.indexOf(this.author_filter) > -1) && 
      (!this.min_price_filter || item.price >= parseInt(this.min_price_filter)) && 
      (!this.max_price_filter || item.price <= parseInt(this.max_price_filter)));
  }

  filterBooksFreeText(){
    this.bookService.freeTextSearch(this.free_text_filter).pipe(debounceTime(400)).subscribe(res=>this.filtered_books = res)
  }

  toggleSearch(){
    this.freeSearch = !this.freeSearch;
    this.name_filter = "";
    this.author_filter = "";
    this.min_price_filter = "";
    this.max_price_filter = "";
    this.free_text_filter = "";
    this.filtered_books = this.all_books;
  }
}
