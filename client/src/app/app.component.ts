import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { User } from './models/user.model';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { CartService } from './services/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'BookHouse';
  data : any;
  logged_in_user: User

  constructor(private userService:UserService, private router:Router, private socket:Socket, private cart: CartService){
    this.data = cart;
    this.data.cartItems = cart.getCart().length;
  }

  ngOnInit(){
     this.userService.logged_in_user.subscribe(res=>this.logged_in_user = res)
  }

  Logout(){
    this.userService.updateLoggedInUser(null);
    this.userService.unsetToken();
    this.router.navigate(['/login'])
  }
}
