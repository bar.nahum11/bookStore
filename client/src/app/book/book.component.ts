import {Component, OnInit, ViewChild, ElementRef, NgZone} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {BookService} from '../services/book.service';
import {Book} from '../models/book.model';
import {CartService} from '../services/cart.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ValidateActionModalComponent } from '../dialogs/validate-action-modal/validate-action-modal.component'; 
import { EditBookComponent } from '../dialogs/edit-book/edit-book.component';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { OkDialogComponent } from '../dialogs/ok-dialog/ok-dialog.component';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})

export class BookComponent implements OnInit {
  id: String;
  book: any;
  intrest: any;
  request;
  infowindow:object;
  address: string;
  logged_in_user: User;

  constructor(private router: Router, 
    private route: ActivatedRoute, 
    private bookService: BookService, 
    private dialog: MatDialog,
    private ngZone: NgZone,
    private cart: CartService,
    private userService:UserService) { }

  ngOnInit() {
    this.userService.logged_in_user.subscribe(res=>this.logged_in_user = res)
    this.book = new Book();
    this.intrest = {value: 0};
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.bookService.getBook(this.id).subscribe(res => {
        this.book = res;
      }, err => this.router.navigate(['/books']));

      this.bookService.getBookInterest(this.id).subscribe(res => {
        this.intrest = res;
        console.log("res: ", res);
      });
    });
  }

  addToCart() {
    this.cart.addToCart(this.book)
      ? this.cart.success('Product successfully added to cart.')
      : this.cart.error('Product has already been added to cart.');
  }

  deleteBook(){
    let deleteDialogRef = this.dialog.open(ValidateActionModalComponent);

    deleteDialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.bookService.deleteBook(this.id).subscribe(
                result => {this.router.navigate(['/'])});
      }
    });
  }

  editBook()
  {
    let deleteDialogRef = this.dialog.open(EditBookComponent, {data:{...this.book}, width:'50%', height: '600px'});

    deleteDialogRef.afterClosed().subscribe(result => {
      if(result)
      {
        this.book = result;
      } else if(result === null) {
        this.dialog.open(OkDialogComponent, {data:"Error - book doesn't exists!"}).afterClosed().subscribe(()=>{
          this.router.navigate(['/']);
        });

      }
    });
  }
}

