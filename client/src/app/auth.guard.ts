import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { UserService } from './services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router, 
    private userService: UserService) {}

  canActivate(route: ActivatedRouteSnapshot) {
    console.log(this.userService.logged_in_user.value);
    const currUser = this.userService.logged_in_user.value;
    if (currUser) {
      if(route.data.expectedRole != 'admin')
        return true;
      else{
        if(!currUser.isAdmin)
          this.router.navigate(['/']);
        return currUser.isAdmin;
      }
    }

    this.router.navigate(['/']);
    return false;
  }
}
