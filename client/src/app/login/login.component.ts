import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User;

  constructor(private userService:UserService, private router:Router, private cartService:CartService) { }

  ngOnInit() {
    this.user = new User();
  }

  Login(){
    this.userService.loginUser(this.user).subscribe(res=>{
      if(res.email) {
        this.userService.updateLoggedInUser(res);
        this.router.navigate(['/books'])
      } else {
        this.cartService.error("Email or password incorrect!");
      }
    });
  }
}
