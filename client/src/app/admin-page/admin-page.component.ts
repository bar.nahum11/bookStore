import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { Socket } from 'ngx-socket-io';
import { OrderService } from '../services/order.service'

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {
  graphData: any;
  currentOnline: number;
  ordersByUser: Object[]

  constructor(private adminService:AdminService, private socket:Socket, private orderService:OrderService) { }

  ngOnInit() {
    this.adminService.NumBooksPerCategory().subscribe(res=> this.graphData = res);
    this.orderService.getNumOfOrdersPerUser().subscribe(
      res=>{
        this.ordersByUser = res
      });
    this.socket.emit('request_user_count');
    this.socket.on('update_user_count', (payload)=>this.currentOnline = payload);
  }

}
