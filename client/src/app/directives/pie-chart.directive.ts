import { Directive, ElementRef, Input } from '@angular/core';
import { select, scaleOrdinal, schemeCategory10, pie, arc } from 'd3';
import 'd3'

@Directive({
  selector: '[appPieChart]'
})
export class PieChartDirective {
  @Input() rawData: Array<any>;
  @Input() width: number;
  @Input() height: number;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    let rawData = this.rawData
    var width = this.width,
        height = this.height,
        radius = Math.min(this.width, this.height) / 2;

    var color = scaleOrdinal(schemeCategory10);

    var valueArc = arc()
        .outerRadius(radius)
        .innerRadius(0);

    var countArc = arc()
        .outerRadius(Math.min(width, height) / 2 * 0.8)
        .innerRadius(Math.min(width, height) / 2 * 0.8);

    // var textArc = arc()
    //     .outerRadius(Math.min(width, height) / 2 * 0.6)
    //     .innerRadius(Math.min(width, height) / 2 * 0.6);

    var pie1 = pie()
        .sort(null)
        .value(function (d:any) { return d.count; });

    var svg = select(this.el.nativeElement).append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var g = svg.selectAll(".arc")
        .data(pie1(rawData))
        .enter().append("g")
        .attr("class", "arc");

    g.append("path")
        .attr("d", function(dd:any){return valueArc(dd)})
        .style("fill", function (d) { return color(d["data"]["name"]); });

     g.append("text")
        .attr("transform", function (d:any) {                
            d.innerRadius = 0;
            d.outerRadius = radius;
            return "translate(" + countArc.centroid(d) + ")";       
        })
        .attr("text-anchor", "middle")                        
        .text(function (d, i) { return rawData[i].count });
    
    // g.append("text")
    //     .attr("transform", function (d:any) {                
    //         d.innerRadius = 0;
    //         d.outerRadius = radius;
    //         return "translate(" + textArc.centroid(d) + ")";       
    //     })
    //     .attr("text-anchor", "middle")                        
    //     .text(function (d, i) { return rawData[i].name });
    
    g.append("title")                        
        .text(function (d, i) { return rawData[i].name });              
  }
}
